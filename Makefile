CC = gcc
CFLAGS = -W -Wall
LDFLAGS = -lssl -lcrypto
SRCDIR = src
SRCFILES = $(wildcard $(SRCDIR)/*.c) 
HFILES = $(wildcard $(SRCDIR)/*.h) 
OBJFILES = src/Compression/compression.o
OBJ = $(wildcard *.o)
SUBDIRS := $(wildcard $(SRCDIR)/*/)
TARGET = main

all : $(SUBDIRS) objects $(TARGET) $(OBJFILES)

.PHONY : subdirs $(SUBDIRS)

subdirs : $(SUBDIRS)

$(SUBDIRS): 
		$(MAKE) -C $@ 

$(TARGET) : $(OBJFILES) $(OBJ)
	$(CC) -g -o $(TARGET) $(OBJFILES) $(OBJ) $(CFLAGS) $(LDFLAGS)

objects : $(SRCFILES) $(HFILES)
	$(CC) -g -c $(SRCFILES) $(CFLAGS) $(LDFLAGS)

clean : 
	rm -rf $(OBJFILES) = $(TARGET)

rmproper : clean 
	rm -rf $(TARGET)
