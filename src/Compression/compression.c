#include "compression.h"

void rle_encode(FILE * fp_in, FILE * fp_out)
{
	long lSize;
	char * buffer;
	char * binary_buffer;
	char * compressed_buffer;
	size_t result;

	if(fp_in == NULL)
	{
		perror("Error while opening the file.\n");
		fprintf(stderr, "Error opening  : '%s'\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	else {
		// get file size
		fseek(fp_in,0, SEEK_END);
		lSize = ftell(fp_in);
		printf("fp_in size : %ld\n", lSize);
		rewind(fp_in);

		buffer = (char *) malloc(sizeof(char)*lSize);

		result = fread(buffer,1, lSize, fp_in);
		if (result != lSize) { fputs("Reading error", stderr); exit(3);}

		char * pb = buffer;
		char * cb = buffer;
		unsigned int index = 0, count = 1;
		for(int i = 0; i<lSize; i++, cb++) {
			if(*cb == *(cb+1)) {
				count++;
			}
			else {
				putc(count, fp_out);
				putc(*cb, fp_out);
				count = 1;
			}
		}
		printf("\n");
	}
	lSize = ftell(fp_out);
	printf("fp_out size : %ld\n", lSize);
	free(buffer);
	fclose(fp_in);
	fclose(fp_out);
}

void rle_decode(FILE * fp_in, FILE * fp_out) {

	long lSize;
	char * buffer;
	char * binary_buffer;
	size_t result;
	if(fp_in == NULL)
	{
		perror("Error while opening the file.\n");
		fprintf(stderr, "Error opening  : '%s'\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	else {

		// get file size
		fseek(fp_in,0, SEEK_END);
		lSize = ftell(fp_in);
		printf("lSize : %ld\n", lSize);
		rewind(fp_in);

		buffer = (char *) malloc(sizeof(char)*lSize);
		result = fread(buffer,1, lSize, fp_in);
		if (result != lSize) { fputs("Reading error", stderr); exit(3);}

		if(fp_out == NULL)
		{
			perror("Error while opening the file.\n");
			fprintf(stderr, "Error opening  : '%s'\n", strerror(errno));
			exit(EXIT_FAILURE);
		}
		else {
			printf("decompressed buffer is : \n");
			for(int i = 0; i<lSize; i++, ++buffer) {
				int run_length = *buffer;
				int value = *++buffer;
				for(int j = 0; j<run_length; j++) {
					/* printf("%X", value); */
					putc(value, fp_out);
				}
			}
		}
		lSize = ftell(fp_out);
		printf("fp_out size : %ld\n", lSize);
	}
}
