#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <openssl/aes.h>
#include <openssl/crypto.h>

void rle_encode(FILE * fp_in, FILE * fp_out);
void rle_decode(FILE * fp_in, FILE * fp_out);
