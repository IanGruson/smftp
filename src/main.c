#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/aes.h>
#include <openssl/crypto.h>
#include "Compression/compression.h"

int main(int argc, char *argv[])
{

	FILE * fp_in, * fp_out;
	int c;
	char ** a; 
	while(--argc > 0 && (*++argv)[0] == '-') {
		while ((c = *++argv[0])) {
			switch (c) {
				case 'c' : 
					a = argv;
					/* printf("Entering compression"); */
					/* printf("fp_in : %s\n", *++a); */
					/* printf("fp_out : %s\n", *++a); */
					/* printf("fp_in : %s", *++argv); */
					/* printf("fp_out : %s", *++argv); */
					fp_in = fopen(*++a, "rb");
					fp_out = fopen(*++a, "wb");
					rle_encode(fp_in, fp_out);
					break;
				case 'd' : 
					a = argv;
					/* printf("Entering decompression"); */
					/* printf("fp_in : %s", *++argv); */
					/* printf("fp_out : %s", *++argv); */
					fp_in = fopen(*++a, "rb");
					fp_out = fopen(*++a, "wb");
					rle_decode(fp_in, fp_out);
					break;
				default :
					printf("Flag %c not recognized.\n", c);
					printf("Flags : \n");
					printf("-c		--compress input file to an output file.\n");
					printf("-d 		--decompress input file to output file.\n");
					argc = 0;
					break; 
			}
		}
	}



	return 0;
}




